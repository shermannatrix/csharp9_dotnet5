﻿using System;
using static System.Console;

namespace CardinalToOrdinal
{
    class Program
    {
        static string CardinalToOrdinal(int number) {
            switch (number) {
                case 11:
                case 12:
                case 13:
                    return $"{number}th";
                default:
                    int lastDigit = number % 10;

					string suffix = lastDigit switch {
						1 => "st",
						2 => "nd",
						3 => "rd",
						_ => "th"
					};
					return $"{number}{suffix}";
            }
        }

		static void RunCardinalToOrdinal() {
			for (int number = 1; number <= 40; number++) {
				Write($"{CardinalToOrdinal(number)} ");
			}
			WriteLine();
		}

        static void Main(string[] args)
        {
           RunCardinalToOrdinal(); 
        }
    }
}
